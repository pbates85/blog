## Git Push from Pipeline

- generate RSA public private key pair
- add public key as a Project Deploy Key with write access
- add private key to the repo via CI variable such as SSH_PUSH_KEY
- gather info on remote host using key scan ``ssh-keyscan <gitlab-host>``
- add results to a CI variable such as CI_KNOWN_HOSTS
- see `deploy-staging.yml` for the scripting portion
package com.trident.blog.resource;


import com.trident.blog.models.Post;
import com.trident.blog.service.PostService;
import org.apache.catalina.connector.Response;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/post/api/")
public class PostResource {

    // Service access
    private PostService postService;

    // create post
//    @PostMapping("/create")
//    public ResponseEntity<Post> create(@ResponseBody Post post)throws IOException {
//        Post newPost(postService.createPost(post.getTitle(), post.getBody()));
//
//    }

    // get all posts
    @GetMapping("/list")
    public ResponseEntity<List<Post>> getAllPosts(){
        List<Post> posts = postService.getAllPosts();
        return new ResponseEntity<>(posts, HttpStatusCode.valueOf(Response.SC_OK));
    }


}

package com.trident.blog.service.impl;

import com.trident.blog.models.Post;
import com.trident.blog.repositories.PostRepository;
import com.trident.blog.service.PostService;

import java.time.LocalDateTime;
import java.util.List;

public class PostServiceImpl implements PostService {

    // db access
    private final PostRepository postRepository;

    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Post createPost(String title, String body) {
        Post post = new Post();
        // set object attributes
        post.setTitle(title);
        post.setBody(body);
        post.setCreatedAt(LocalDateTime.now());

        // save post
        postRepository.save(post);
        return post;
    }

    @Override
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @Override
    public Post getPostById(Long id) {
        return postRepository.getReferenceById(id);
    }

    @Override
    public Post updatePost(Long id, String newTitle, String newBody) {
        Post currentPost = postRepository.getReferenceById(id);
        currentPost.setTitle(newTitle);
        currentPost.setBody(newBody);
        // save new post
        return postRepository.save(currentPost);
    }

    @Override
    public void deletePost(Long id) {
        // retrieve post from db
        Post post = postRepository.getReferenceById(id);
        postRepository.delete(post);
    }
}

package com.trident.blog.service;


import com.trident.blog.models.Post;

import java.util.List;

public interface PostService {

    // create posts
    Post createPost(String title, String body);

    // get all posts
    List<Post> getAllPosts();

    // get post by id
    Post getPostById(Long id);

    // update post
    Post updatePost(Long id, String newTitle, String newBody);

    // delete post
    void deletePost(Long id);

}
